//
//  RootViewController.h
//  project_eat
//
//  Created by xxoo on 14-11-4.
//  Copyright (c) 2014年 linktop. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum layerState
{
    STATE_OUT_LEFT,
    STATE_OUT_RIGHT,
    STATE_IN_LEFT,
    STATE_IN_RIGHT,
    STATE_NONE
}LAYERSTATE;

@interface RootViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) UIScrollView *scrollview;
@property (strong, nonatomic) UIPageControl * pagecontrol;

@end

