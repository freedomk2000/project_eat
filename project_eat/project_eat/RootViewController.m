//
//  RootViewController.m
//  project_eat
//
//  Created by xxoo on 14-11-4.
//  Copyright (c) 2014年 linktop. All rights reserved.
//

#import "RootViewController.h"
#import "VCUser.h"
#import "VCUserSetting.h"
#import "VCEat.h"
//#import "SDBManager.h"

#import "XSqlAPI.h"


@interface RootViewController ()
{
//    UIButton * button_eat;
//    UIButton * button_user;
//    UIButton * button_eat;
    float initialX;
    Boolean EndDraging;
//    CALayer * layer_eat;
//    CALayer * layer_user;
//    CALayer * layer_wiki;
    NSMutableArray * data;
}

//@property (readonly, strong, nonatomic) ModelController *modelController;
@end

@implementation RootViewController

//@synthesize modelController = _modelController;


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // Configure the page view controller and add it as a child view controller.
    EndDraging = YES;
    self.scrollview = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    
    [self.scrollview setContentSize:CGSizeMake(self.view.frame.size.width*3, self.view.frame.size.height)];//设置scrollview画布的大小，此设置为三页的宽度，380的高度。用来实现三页照片的转换。
    self.scrollview.pagingEnabled=YES;//立刻翻页到下一页 没中间的拖动过程
        self.scrollview.bounces=NO;//去掉翻页中的白屏
    [self.scrollview setDelegate:self];
    self.scrollview.showsHorizontalScrollIndicator= NO;//不现实水平滚动条
//    self.scrollview.showsVerticalScrollIndicator = YES;
    self.scrollview.backgroundColor = [UIColor whiteColor];
//    self.scrollview.zoomScale = 0.5;
    
    
    
    
    UIButton* button_eat = [UIButton buttonWithType:UIButtonTypeCustom];
    button_eat.frame = CGRectMake((self.view.bounds.size.width-100)/2, self.view.bounds.size.height/2, 100, 50);
    button_eat.backgroundColor = [UIColor yellowColor];
//    button_eat.titleLabel.text = @"膳食计划";
    [button_eat setTitle:@"膳食计划" forState:UIControlStateNormal];
    [button_eat setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button_eat setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    [button_eat addTarget:self action:@selector(fun_eat:) forControlEvents:UIControlEventTouchUpInside];
    CALayer * layer_eat = [[CALayer alloc] init];
    layer_eat.frame = self.view.frame;
    layer_eat.backgroundColor = [UIColor yellowColor].CGColor;
//    [layer_eat addSublayer:button_eat.layer];
    [self.scrollview.layer addSublayer:layer_eat];
    [self.scrollview addSubview:button_eat];
    NSDictionary * dic_eat = [[NSDictionary alloc] initWithObjectsAndKeys:button_eat,@"button",layer_eat,@"layer", nil];

    
    UIButton * button_user = [UIButton buttonWithType:UIButtonTypeCustom];
    button_user.frame = CGRectMake((self.view.frame.size.width-100)/2+self.view.bounds.size.width, self.view.frame.size.height/2, 100, 50);
    button_user.backgroundColor = [UIColor yellowColor];
    [button_user setTitle:@"用户设置" forState:UIControlStateNormal];
    [button_user setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button_user setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    [button_user addTarget:self action:@selector(fun_user:) forControlEvents:UIControlEventTouchUpInside];
    CALayer * layer_user = [[CALayer alloc] init];
    layer_user.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.bounds.size.height);
    layer_user.backgroundColor = [UIColor grayColor].CGColor;
//    [layer_user addSublayer:button_user.layer];
    [self.scrollview.layer addSublayer:layer_user];
    [self.scrollview addSubview:button_user];
    NSDictionary * dic_user = [[NSDictionary alloc] initWithObjectsAndKeys:button_user,@"button",layer_user,@"layer", nil];
    
    
    CALayer * layer_wiki = [[CALayer alloc] init];
    layer_wiki.frame = CGRectMake(self.view.frame.size.width*2, 0, self.view.frame.size.width, self.view.bounds.size.height);;
    layer_wiki.backgroundColor = [UIColor grayColor].CGColor;
//    [layer_wiki addSublayer:button_user.layer];
    [self.scrollview.layer addSublayer:layer_wiki];
//    [self.scrollview addSubview:button_user];
    NSDictionary * dic_wiki = [[NSDictionary alloc] initWithObjectsAndKeys:layer_wiki,@"layer", nil];
    
    
    self.pagecontrol = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-30, self.view.frame.size.width, 30)];
    self.pagecontrol.numberOfPages=3;
    self.pagecontrol.currentPage=0;
    self.pagecontrol.pageIndicatorTintColor = [UIColor grayColor];
    self.pagecontrol.currentPageIndicatorTintColor = [UIColor blackColor];
    //    self.pagecontrol.tintColor = [UIColor greenColor];
//    self.pagecontrol.backgroundColor = [UIColor blueColor];
    [self.pagecontrol addTarget:self action:@selector(pageturn:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:self.scrollview];
    [self.view addSubview:self.pagecontrol];

//    [[XSqlAPI SharedInstance] insertnu];
    [[XSqlAPI SharedInstance] ShoeOfMac:@"sdf"];
    
//    NSString * query = [NSString stringWithFormat:@"deviceID = '%@' AND longitude = %@ AND latitude = %@",self.deviceID,self.longitude,self.latitude];
//    NSArray * ret = [[SDBManager defaultDBManager] findWithUid:nil limit:100 TableName:@"Nutriention"];
    
    data = [[NSMutableArray alloc] initWithObjects:dic_eat,dic_user,dic_wiki, nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)pageturn:(id)sender
{
    CGSize viewsize=self.scrollview.frame.size;
    CGRect rect=CGRectMake(((UIPageControl*)sender).currentPage*viewsize.width, 0, viewsize.width, viewsize.height);
    [self.scrollview scrollRectToVisible:rect animated:YES];
}


-(void)fun_eat:(id)sender
{
    VCEat * eat = [[VCEat alloc] init];
    [self.navigationController pushViewController:eat animated:YES];
}







-(void)fun_user:(id)sender
{
    VCUser * user = [self.storyboard instantiateViewControllerWithIdentifier:@"ID_VCUser"];
    [self.navigationController pushViewController:user animated:YES];
//    VCUserSetting * userSetting = [self.storyboard instantiateViewControllerWithIdentifier:@"ID_VCUserSetting"];
//    [self.navigationController pushViewController:userSetting animated:YES];

}


-(void)ChangingPage:(UIPanGestureRecognizer*)sender
{
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        initialX = self.scrollview.contentOffset.x;
    }
    else if (sender.state == UIGestureRecognizerStateChanged)
    {
        NSLog(@"111");
    }
    else if (sender.state == UIGestureRecognizerStateEnded)
    {
        
    }
    else{}
//    self.scrollview.contentOffset = CGPointMake(self., <#CGFloat y#>)
}


#pragma mark scrollview delegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView1{
    CGPoint offset=scrollView1.contentOffset;
    CGRect bounds=scrollView1.frame;
    [self.pagecontrol setCurrentPage:offset.x/bounds.size.width];
    
//    int index = self.pagecontrol.currentPage;
//    if(data.count > index+1)
//    {
//        NSDictionary * dic_next = [data objectAtIndex:index+1];
//        CALayer * layer = [dic_next objectForKey:@"layer"];
//        
//        
//        CGPoint oldanchor = layer.anchorPoint;
//        [layer setAnchorPoint:CGPointMake(1, 0.5)];
//        //    [layer_eat setPosition:CGPointMake(0, 0)];
//        [layer setPosition:CGPointMake(layer.position.x - layer.frame.size.width * (oldanchor.x - layer.anchorPoint.x), layer.position.y - layer.frame.size.height * (layer.anchorPoint.y - oldanchor.y))];
//        layer.fillMode = kCAFillModeForwards;
//        CATransform3D rotate = CATransform3DMakeRotation(1, 0, -1, 0);
//        layer.transform = CATransform3DPerspect(rotate,CGPointMake(0, 0),500);
//    }
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
//    initialX = self.scrollview.contentOffset.x;
    EndDraging = NO;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    EndDraging = YES;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int index_scroll = scrollView.contentOffset.x / self.view.frame.size.width;
    int index = self.pagecontrol.currentPage;
    if(index+1 == index_scroll)
        return;
    NSLog(@"index:%d,index_scroll:%d",index,index_scroll);
    NSDictionary * dic_cur = [data objectAtIndex:index];
    CALayer * layer = [dic_cur objectForKey:@"layer"];
    UIButton * button = [dic_cur objectForKey:@"button"];
    if(index <= index_scroll)
    {
        [self SetLayerState:layer State:STATE_OUT_RIGHT];
        [self SetLayerState:button.layer State:STATE_OUT_RIGHT];
        if(data.count > index+1)
        {
            NSDictionary * dic_next = [data objectAtIndex:index+1];
            CALayer * layer_next = [dic_next objectForKey:@"layer"];
            UIButton * button_next = [dic_next objectForKey:@"button"];
            [self SetLayerState:layer_next State:STATE_IN_LEFT];
            [self SetLayerState:button_next.layer State:STATE_IN_LEFT];
        }
    }
    else if(index > index_scroll)
    {
        [self SetLayerState:layer State:STATE_OUT_LEFT];
        [self SetLayerState:button.layer State:STATE_OUT_LEFT];
        NSDictionary * dic_pre = [data objectAtIndex:index-1];
        CALayer * layer_pre = [dic_pre objectForKey:@"layer"];
        UIButton * button_pre = [dic_pre objectForKey:@"button"];
        [self SetLayerState:layer_pre State:STATE_IN_RIGHT];
        [self SetLayerState:button_pre.layer State:STATE_IN_RIGHT];
    }
}

-(void)SetLayerState:(CALayer*)layer State:(LAYERSTATE)state
{
    float tmp = (int)self.scrollview.contentOffset.x % (int)self.view.frame.size.width;
    float use;
    CGPoint position;
    int y;
    switch (state) {
        case STATE_OUT_LEFT:
            use = (self.view.frame.size.width - tmp)/self.view.frame.size.width;
            position = CGPointMake(1, 0.5);
            y = -1;
            break;
        case STATE_OUT_RIGHT:
            use = tmp/self.view.frame.size.width;
            position = CGPointMake(0, 0.5);
            y = 1;
            
            break;
        case STATE_IN_LEFT:
            use = (self.view.frame.size.width - tmp)/self.view.frame.size.width;
            position = CGPointMake(1, 0.5);
            y = -1;
            break;
        case STATE_IN_RIGHT:
            use = tmp/self.view.frame.size.width;
            position = CGPointMake(0, 0.5);
            y = 1;
            break;
        default:
            break;
    }
    CGPoint oldanchor = layer.anchorPoint;
    [layer setAnchorPoint:position];
    [layer setPosition:CGPointMake(layer.position.x - layer.frame.size.width * (oldanchor.x - layer.anchorPoint.x), layer.position.y - layer.frame.size.height * (oldanchor.y - layer.anchorPoint.y))];
    layer.fillMode = kCAFillModeForwards;
    CATransform3D rotate = CATransform3DMakeRotation(use, 0, y, 0);
    layer.transform = CATransform3DPerspect(rotate,CGPointMake(0, 0),500);
    
}


CATransform3D CATransform3DPerspect(CATransform3D t, CGPoint center, float disZ)
{
    CATransform3D transToCenter = CATransform3DMakeTranslation(center.x,center.y, 0);
    CATransform3D translateBack = CATransform3DMakeTranslation(-center.x, -center.y, 0);
    CATransform3D scale = CATransform3DIdentity;
    scale.m34 = -1.0f/disZ;
    CATransform3D cat1 = CATransform3DConcat(CATransform3DConcat(transToCenter,scale), translateBack);
    return CATransform3DConcat(t,cat1);
}


@end
