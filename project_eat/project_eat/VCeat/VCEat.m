//
//  VCEat.m
//  project_eat
//
//  Created by xxoo on 14-11-8.
//  Copyright (c) 2014年 linktop. All rights reserved.
//

#import "VCEat.h"
#import "VCEat1.h"

@interface VCEat ()

@end

@implementation VCEat

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton * BTbegineat = [UIButton buttonWithType:UIButtonTypeCustom];
    BTbegineat.frame = CGRectMake((self.view.frame.size.width-100)/2, self.view.frame.size.height/2, 100, 50);
    BTbegineat.backgroundColor = [UIColor yellowColor];
    [BTbegineat setTitle:@"开始召唤" forState:UIControlStateNormal];
    [BTbegineat setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [BTbegineat setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    [BTbegineat addTarget:self action:@selector(fun_begineat:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:BTbegineat];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)fun_begineat:(id)sender
{
    VCEat1 * vceat1 = [[VCEat1 alloc] init];
    [self.navigationController pushViewController:vceat1 animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
