//
//  VCEat1.h
//  project_eat
//
//  Created by xxoo on 14-11-8.
//  Copyright (c) 2014年 linktop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCEat1 : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *Button1;
@property (weak, nonatomic) IBOutlet UIButton *Button2;
@property (weak, nonatomic) IBOutlet UIButton *Button3;
@property (weak, nonatomic) IBOutlet UIButton *Button4;
@property (weak, nonatomic) IBOutlet UIButton *Button5;
@property (weak, nonatomic) IBOutlet UIButton *Button6;
@property (weak, nonatomic) IBOutlet UIButton *ButtonReturn;
@property (weak, nonatomic) IBOutlet UIButton *ButtonNext;
- (IBAction)Button_fun:(id)sender;

@end
