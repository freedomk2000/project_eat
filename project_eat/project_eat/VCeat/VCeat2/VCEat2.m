//
//  VCEat2.m
//  project_eat
//
//  Created by xxoo on 14-11-8.
//  Copyright (c) 2014年 linktop. All rights reserved.
//

#import "VCEat2.h"
#import "VCEat2cellTableViewCell.h"
#import "VCEat3.h"


@interface VCEat2 ()
{
    NSArray * datasource;
}

@end

@implementation VCEat2

- (void)viewDidLoad {
    [super viewDidLoad];
    datasource = [[NSArray alloc] initWithObjects:@"海鲜",@"肉食",@"素食",@"奢华",@"清淡",@"随便",@"确定", nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark tableviewdelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"eat2tableviewcell2";
    VCEat2cellTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"VCEat2cellTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    cell.mylabel.text =[datasource objectAtIndex:indexPath.row];
    if(datasource.count -1 == indexPath.row)
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, 60);
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == datasource.count-1)
    {
        VCEat3 * eat3 = [[VCEat3 alloc] init];
        [self.navigationController pushViewController:eat3 animated:YES];
    }
}

@end
