//
//  VCEat3.h
//  project_eat
//
//  Created by xxoo on 14-11-14.
//  Copyright (c) 2014年 linktop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCHCircleView.h"

@interface VCEat3 : UIViewController<SCHCircleViewDataSource,SCHCircleViewDelegate>
{
    SCHCircleView   *_circle_view;
    
    NSMutableArray  *_food_array;
}

@end
