//
//  VCEat3.m
//  project_eat
//
//  Created by xxoo on 14-11-14.
//  Copyright (c) 2014年 linktop. All rights reserved.
//

#import "VCEat3.h"
#import "viewCell.h"

#define circleRadius   300

@interface VCEat3 ()

@end

@implementation VCEat3

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDictionary * dic1 = [[NSDictionary alloc] initWithObjectsAndKeys:[UIImage imageNamed:@"1"],@"image",@"test1",@"name", nil];
    NSDictionary * dic2 = [[NSDictionary alloc] initWithObjectsAndKeys:[UIImage imageNamed:@"2"],@"image",@"test2",@"name", nil];
    NSDictionary * dic3 = [[NSDictionary alloc] initWithObjectsAndKeys:[UIImage imageNamed:@"3"],@"image",@"test3",@"name", nil];
    NSDictionary * dic4 = [[NSDictionary alloc] initWithObjectsAndKeys:[UIImage imageNamed:@"4"],@"image",@"test4",@"name", nil];
    NSDictionary * dic5 = [[NSDictionary alloc] initWithObjectsAndKeys:[UIImage imageNamed:@"5"],@"image",@"test5",@"name", nil];
    _food_array = [[NSMutableArray alloc] initWithObjects:dic1,dic2,dic3,dic4,dic5,nil];
    _circle_view = [[SCHCircleView alloc] initWithFrame:CGRectMake(50, 80, circleRadius, circleRadius)];
    [self.view addSubview:_circle_view];
//    NSLog(@"x:%f,y:%f,wi:%f,he:%f",self.view.bounds.origin.x,self.view.bounds.origin.y,self.view.bounds.size.width,self.view.bounds.size.height);
    _circle_view.circle_view_data_source = self;
    _circle_view.circle_view_delegate    = self;
    _circle_view.show_circle_style       = SChShowCircleWinding;
//    _circle_view.circle_delete_style = SCHCircleDeleteTwinkle;
    [_circle_view reloadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark - SCHCircleViewDataSource


- (CGPoint)centerOfCircleView:(SCHCircleView *)circle_view
{
    return CGPointMake(160.0f, 165.0f);
}

- (NSInteger)numberOfCellInCircleView:(SCHCircleView *)circle_view
{
    return _food_array.count;
}

- (SCHCircleViewCell *)circleView:(SCHCircleView *)circle_view cellAtIndex:(NSInteger)index_circle_cell
{
    viewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"viewCell" owner:nil options:nil] lastObject];
    
    [cell.image_view setImage:[[_food_array objectAtIndex:index_circle_cell] objectForKey:@"image"]];
    [cell.name setText:[[_food_array objectAtIndex:index_circle_cell] objectForKey:@"name"]];
    UILabel * test = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    [test setText:@"nimei"];
    [cell addSubview:test];
    return cell;
}

#pragma mark -
#pragma mark - SCHCircleViewDelegate
- (void)touchEndCircleViewCell:(SCHCircleViewCell *)cell indexOfCircleViewCell:(NSInteger)index
{
    NSLog(@"%d",index);
}




@end
