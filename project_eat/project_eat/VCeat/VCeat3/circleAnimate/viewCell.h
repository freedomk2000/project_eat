//
//  viewCell.h
//  SCHCricleView
//
//  Created by 魏巍 on 12-11-11.
//  Copyright (c) 2012年 sch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCHCircleViewCell.h"
@interface viewCell : SCHCircleViewCell
{
    UIImageView *_image_view;
    UILabel * _name;
}
@property (retain, nonatomic) IBOutlet UILabel *name;
@property (nonatomic,retain) IBOutlet UIImageView *image_view;
@end
