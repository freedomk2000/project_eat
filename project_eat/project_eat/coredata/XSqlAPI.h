//
//  XSqlAPI.h
//  Xstep
//
//  Created by linktop on 13-9-29.
//  Copyright (c) 2013年 linktop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShoeID.h"


@interface XSqlAPI : NSObject 
{
    NSInteger SaveTimeCount;
    NSManagedObjectContext *_managedObjectContext;
    NSManagedObjectModel *_managedObjectModel;
    NSPersistentStoreCoordinator *_persistentStoreCoordinator;
    
    NSLock * lock;
    
}
//@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
//@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
//@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

-(id)init;
+(id)SharedInstance;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

//shoetouser
-(Boolean)insertnu;
-(Nutriention *)ShoeOfMac:(NSString *)shoemac;


//logfile
-(void) writeLogFile:(NSString *) filename Message:(NSString *)message;
-(NSString *) readLogFile:(NSString *)filename;
-(Boolean)removeLogFile:(NSString *)filename;


-(Boolean)RecountPersonalInfo:(NSString *)name;
@end
