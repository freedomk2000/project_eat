//
//  XSqlAPI.m
//  Xstep
//
//  Created by linktop on 13-9-29.
//  Copyright (c) 2013年 linktop. All rights reserved.
//

#import "XSqlAPI.h"



static XSqlAPI * instance;
@implementation XSqlAPI

//@synthesize managedObjectContext = _managedObjectContext;
//@synthesize managedObjectModel = _managedObjectModel;
//@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+(id)SharedInstance
{
    if(!instance)
    {
        instance = [[self alloc] init];
    }
    return  instance;
}



-(id)init
{
    self = [super init];
    if(self)
    {
        SaveTimeCount = 0;
        lock = [[NSLock alloc] init];
    }
    return self;
}

-(void)dealloc
{

}







#pragma mark ShoeID

-(Boolean)insertnu
{
    [lock lock];
    NSEntityDescription *object=[NSEntityDescription entityForName:@"Nutriention" inManagedObjectContext:[self managedObjectContext]];
    Nutriention * tmp = [[Nutriention alloc] initWithEntity:object insertIntoManagedObjectContext:[self managedObjectContext]];
    tmp.n_id = [NSNumber numberWithInt:1];
    tmp.f_id = [NSNumber numberWithInt:2];
    tmp.introducement = @"sdfsd";
    tmp.nutriention_name = @"sdfwrt";
    [self saveContext];
    [lock unlock];
    return YES;
}


-(Nutriention *)ShoeOfMac:(NSString *)shoemac
{
    [lock lock];
    NSLog(@"shoemac:%@",shoemac);
    NSError *error;
    NSArray *fetchresult;
    NSFetchRequest *fetch=[[NSFetchRequest alloc]init];
    NSEntityDescription *object=[NSEntityDescription entityForName:@"Nutriention" inManagedObjectContext:[self managedObjectContext]];
    [fetch setEntity: object];
    [fetch setPredicate:nil];
    fetchresult = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    NSLog(@"fetch.count:%d",fetchresult.count);
    for (Nutriention * obj in fetchresult) {
        NSLog(@"id:%d,mark:%@,intro:%@",obj.n_id,obj.mark,obj.introducement);
    }
//    if(fetchresult.count)
//    {
//        ShoeID * tmpdata = [fetchresult objectAtIndex:0];
//        NSLog(@"name:%@, mac:%@, male:%d",tmpdata.username,tmpdata.shoemac,tmpdata.gender.intValue);
//        [lock unlock];
//        return tmpdata;
//    }
    [lock unlock];
    return nil;
}





- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] )
            if(![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}



#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    
//    if([NSThread isMainThread])
    {
        if (_managedObjectContext != nil) {
            return _managedObjectContext;
        }
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (coordinator != nil) {
            _managedObjectContext = [[NSManagedObjectContext alloc] init];
            [_managedObjectContext setPersistentStoreCoordinator:coordinator];
        }
        return _managedObjectContext;
    }
//    else
//    {
//        NSManagedObjectContext * MymanagedObjectContext = nil;
//        
//        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
//        if (coordinator != nil) {
//            MymanagedObjectContext = [[NSManagedObjectContext alloc] init];
//            [MymanagedObjectContext setPersistentStoreCoordinator:coordinator];
//        }
//        return MymanagedObjectContext;
//    }
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
//    if([NSThread isMainThread])
    {
        if (_managedObjectModel != nil) {
            return _managedObjectModel;
        }
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
        _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        return _managedObjectModel;
    }
//    else
//    {
//        NSManagedObjectModel *MymanagedObjectModel = nil;
//        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Xtep_arc" withExtension:@"momd"];
//        MymanagedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
//        return MymanagedObjectModel;
//    }
    
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
//    if([NSThread isMainThread])
    {
        if (_persistentStoreCoordinator != nil) {
            return _persistentStoreCoordinator;
        }
        
        NSFileManager*fileManager =[NSFileManager defaultManager];
        NSError*error;
        NSArray*paths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString*documentsDirectory =[paths objectAtIndex:0];
        
        NSString*resourcePath =[[NSBundle mainBundle] pathForResource:@"base" ofType:@"db"];
        NSString*txtPath =[documentsDirectory stringByAppendingPathComponent:@"base.db"];
        
        while([fileManager fileExistsAtPath:txtPath]== NO){
            
            if([fileManager copyItemAtPath:resourcePath toPath:txtPath error:&error])
                NSLog(@"sdf2");
        }
        
        
        
        
//        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"base.db"];
        NSURL * test = [NSURL fileURLWithPath:txtPath];
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[NSURL fileURLWithPath:txtPath] options:nil error:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             
             Typical reasons for an error here include:
             * The persistent store is not accessible;
             * The schema for the persistent store is incompatible with current managed object model.
             Check the error message to determine what the actual problem was.
             
             
             If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
             
             If you encounter schema incompatibility errors during development, you can reduce their frequency by:
             * Simply deleting the existing store:
             [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
             
             * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
             @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
             
             Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
             
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        return _persistentStoreCoordinator;
    }
//    else
//    {
//        NSPersistentStoreCoordinator *MypersistentStoreCoordinator = nil;
//        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Xtep_arc.sqlite"];
//        
//        NSError *error = nil;
//        MypersistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
//        if (![MypersistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
//            /*
//             Replace this implementation with code to handle the error appropriately.
//             
//             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//             
//             Typical reasons for an error here include:
//             * The persistent store is not accessible;
//             * The schema for the persistent store is incompatible with current managed object model.
//             Check the error message to determine what the actual problem was.
//             
//             
//             If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
//             
//             If you encounter schema incompatibility errors during development, you can reduce their frequency by:
//             * Simply deleting the existing store:
//             [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
//             
//             * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
//             @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
//             
//             Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
//             
//             */
//            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//            abort();
//        }
//        
//        return MypersistentStoreCoordinator;
//    }
//    if (_persistentStoreCoordinator != nil) {
//        return _persistentStoreCoordinator;
//    }
    
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    NSArray * tmp = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    return [tmp lastObject];
}



#pragma mark logfile

-(void) writeLogFile:(NSString *) filename Message:(NSString *)message
{
    //创建文件管理器
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //获取路径
    //1、参数NSDocumentDirectory要获取的那种路径
    NSArray*  paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    //2、得到相应的Documents的路径
    NSString* DocumentDirectory = [paths objectAtIndex:0];
    //3、更改到待操作的目录下
    [fileManager changeCurrentDirectoryPath:[DocumentDirectory stringByExpandingTildeInPath]];
    //4、创建文件fileName文件名称，contents文件的内容，如果开始没有内容可以设置为nil，attributes文件的属性，初始为nil
    NSString * stirng_last = [self readLogFile:filename];
    [fileManager removeItemAtPath:filename error:nil];
    NSString *path = [DocumentDirectory stringByAppendingPathComponent:filename];
    //5、创建数据缓冲区
    NSMutableData  *writer = [[NSMutableData alloc] init];
    //6、将字符串添加到缓冲中
    [writer appendData:[stirng_last dataUsingEncoding:NSUTF8StringEncoding]];
    [writer appendData:[message dataUsingEncoding:NSUTF8StringEncoding]];
    //7、将其他数据添加到缓冲中
    //将缓冲的数据写入到文件中
    [writer writeToFile:path atomically:YES];
}


-(NSString *) readLogFile:(NSString *)filename
{
    //创建文件管理器
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //获取路径
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    [fileManager changeCurrentDirectoryPath:[documentsDirectory stringByExpandingTildeInPath]];
    
    //获取文件路劲
    NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
    NSData* reader = [NSData dataWithContentsOfFile:path];
    return [[NSString alloc] initWithData:reader encoding:NSUTF8StringEncoding];
}

-(Boolean)removeLogFile:(NSString *)filename
{
    //创建文件管理器
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //获取路径
    //1、参数NSDocumentDirectory要获取的那种路径
    NSArray*  paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    //2、得到相应的Documents的路径
    NSString* DocumentDirectory = [paths objectAtIndex:0];
    //3、更改到待操作的目录下
    [fileManager changeCurrentDirectoryPath:[DocumentDirectory stringByExpandingTildeInPath]];
    //4、创建文件fileName文件名称，contents文件的内容，如果开始没有内容可以设置为nil，attributes文件的属性，初始为nil
    [fileManager removeItemAtPath:filename error:nil];
    return YES;
}



@end
